var gulp = require('gulp'),
    browserSync = require('browser-sync');

gulp.task('sass:reload', ['sass'], function () {
    browserSync.reload();
});