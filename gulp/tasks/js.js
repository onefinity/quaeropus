var gulp         = require('gulp'),
    gulpif       = require('gulp-if'),
    gutil        = require('gulp-util'),
    uglify       = require('gulp-uglify'),
    sourcemaps   = require('gulp-sourcemaps'),
    insert       = require('gulp-insert'),
    watchify     = require('watchify'),
    browserify   = require('browserify'),
    browserSync  = require('browser-sync'),
    source       = require('vinyl-source-stream'),
    buffer       = require('vinyl-buffer'),
    assign       = require('lodash.assign');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').js,
    production   = require('../config').production;

var opts = assign({}, watchify.args, config.browserify);
var bundler = ( production ) ? browserify(opts) : watchify(browserify(opts));

gulp.task('js', ['js:hint'], bundle);
bundler.on('update', bundle); // On any dep update, runs the bundler, watch.js makes sure jshint is executed as well
bundler.on('log', gutil.log); // Output build logs to terminal

function bundle() {
    return bundler.bundle()
        .on('error', handleErrors)
        .pipe(source(config.dest.filename))
        .pipe(buffer())
        .pipe(gulpif(production, uglify()))
        .pipe(gulpif(!production, sourcemaps.init({loadMaps: true}))) // loads map from browserify file
        .pipe(gulpif(!production, sourcemaps.write('./'))) // writes .map file
        .pipe(insert.wrap(config.prepend || '', config.append || ''))
        .pipe(gulp.dest(config.dest.path))
        .pipe(browserSync.reload({
            stream: true
        }));
}