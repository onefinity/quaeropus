var gulp       = require('gulp'),
    imagemin   = require('gulp-imagemin'),
    newer      = require('gulp-newer'),
    pngquant   = require('imagemin-pngquant'),
    del        = require('del'),
    assign     = require('lodash.assign'),
    gutil      = require('gulp-util'),
    dircompare = require('dir-compare');
var config     = require('../config').images;

gulp.task('images', function () {
    return gulp.src(config.src)
        .pipe(newer(config.dest))
        .pipe(imagemin(assign({use: [pngquant()]}, config.imagemin)))
        .pipe(gulp.dest(config.dest))

        // Delete files that are in build but not in source, which might occur when renaming files in source
        .on('end', function() {
            var src = config.src.substr(0, config.src.lastIndexOf('**/'));
            var dest = config.dest;

            var res = dircompare.compareSync(src, dest, {
                compareSize: false,
                compareContent: false,
                skipSubdirs: false
            });

            if ( !res.same ) {
                var files = [];

                res.diffSet.forEach(function(entry) {
                    if ( entry.type1 === 'missing' ) {
                        files.push(config.dest + entry.name2);
                    }
                });

                if ( files ) {
                    gutil.log(gutil.colors.red.bold('Deleting the following images from ' + config.dest + ' because they are not in ' + config.src + ' anymore: \n' + files));
                    del(files);
                }
            }
        });
});