var gulp        = require('gulp'),
    browserSync = require('browser-sync');
var config      = require('../config').browserSync,
    production  = require('../config').production;

gulp.task('browser-sync', function() {
    if ( !production) browserSync(config);
});