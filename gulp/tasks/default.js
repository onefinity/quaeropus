var gulp       = require('gulp'),
    gutil      = require('gulp-util');
var production = require('../config').production;

if ( production ) gutil.log(gutil.colors.blue.bold('Running production tasks, this might take a while...'));

gulp.task('default', ['watch'], function() {
    if ( production ) {
        setTimeout(function() {
            gutil.log(gutil.colors.blue.bold('Production tasks executed successfully ♥'));
        }, 0);
    }
});