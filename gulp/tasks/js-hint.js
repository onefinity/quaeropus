var gulp         = require('gulp'),
    jshint       = require('gulp-jshint');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').js.hint;

gulp.task('js:hint', function () {
    return gulp.src(config.src)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'))

        .on('error', handleErrors);
});