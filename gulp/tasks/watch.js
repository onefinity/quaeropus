var gulp   = require('gulp'),
    gutil  = require('gulp-util');
var config = require('../config');

gulp.task('watch', ['sass', 'js', 'browser-sync'], function() {
    if ( !config.production ) {
        gulp.watch(config.sass.src, ['sass:reload']);
        gulp.watch(config.js.hint.src, ['js:hint']);
        gulp.watch(config.images.src, ['images']);

        gutil.log(gutil.colors.green.bold('Watching for changes...'));
    }
});