var gulp         = require('gulp'),
    gulpif       = require('gulp-if'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    insert       = require('gulp-insert');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').sass,
    production   = require('../config').production;

gulp.task('sass', function () {
    return gulp.src(config.src)
        .on('error', handleErrors)
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(sass(config.settings))
        .pipe(gulpif(!production, sourcemaps.write()))
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(insert.wrap(config.prepend || '', config.append || ''))
        .pipe(gulp.dest(config.dest));
});