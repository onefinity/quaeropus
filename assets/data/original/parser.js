// This is a script for NodeJS
// Run it using `node parser <file>`
var p = require('path'),
    fs = require('fs'),
    readline = require('readline');
var args = process.argv.slice(2),
    path = args[0];

if ( typeof path === 'undefined' ) throw('No file specified');

// Read file line by line through a stream
var rl = readline.createInterface({
    input: fs.createReadStream(path)
});

// Generate output based on lines
var output = {};
rl.on('line', function(line) {
    var path = line.split(';').map(function(v) { return v.replace(/^"(.*)"$/, '$1'); });
    var value = path.pop();

    // Convert to int/float if it's a number
    if ( !isNaN(value) ) value = +value;

    createNestedObject(output, path, value);
});

// When all lines are read and the output is generated save it to file
rl.on('close', function() {
    var parse = p.parse(path);
    var filename = parse.name + '.json';
    fs.writeFile(filename, JSON.stringify(output, null, 4), function(err) {
        if ( err ) throw ( err );
        console.log('JSON saved to %s.', filename);
    });
});

var createNestedObject = function(base, names, value) {
    // If a value is given, remove the last name and keep it for later
    var lastName = arguments.length === 3 ? names.pop() : false;

    // Walk the hierarchy, creating new objects where needed.
    // If the lastName was removed, then the last object is not set yet
    for ( var i = 0; i < names.length; i++ ) {
        base = base[names[i]] = base[names[i]] || {};
    }

    // If a value was given, set it to the last name
    if ( lastName ) base = base[lastName] = value;

    // Return the last object in the hierarchy
    return base;
};