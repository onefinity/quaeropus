var $ = require('jquery'),
    _ = require('lodash'),
    jobs = require('./visualizations.jobs.js'),
    salaries = require('./visualizations.salaries.js');

module.exports = {
    init: function() {
        jobs.init();
        salaries.init();

        $(window).on('resize', function() {
            this.resize();
        }.bind(this));
    },
    draw: function() {
        jobs.draw();
        salaries.draw();
    },
    resize: function() {
        jobs.resize();
        salaries.resize();
    }
};

// Create lodash crush mixin
// http://stackoverflow.com/a/10156831/1358948
_.mixin({
    crush: function(list, shallow, r) {
        var flatten = (r = function(list) {
            return _.isObject(list) ? _.flatten(_.map(list, shallow ? _.identity : r )) : list;
        });

        return _.isObject(list) ? flatten(list) : [];
    }
});