var $ = require('jquery'),
    keycharm = require('keycharm')();

module.exports = {
    init: function() {
        if ( global.DEBUG ) {
            keycharm.bind('up', this.previous);
            keycharm.bind('down', this.next);
        }

        var self = this;
        $('.slide').on('click', '.previous-slide', function(e) {
            e.preventDefault();
            self.previous();
        });
        $('.slide').on('click', '.next-slide', function(e) {
            e.preventDefault();
            if ( !$(this).closest('.slide').hasClass('is-behind') ) self.next();
        });
    },
    previous: function() {
        var $slides = $('.slide.is-active');

        if ( $slides.length > 1 ) $slides.last().removeClass('is-active');
        $('.slide').removeClass('is-behind');
        $('.slide.is-active:not(:last)').addClass('is-behind');
    },
    next: function() {
        var $slide = $('.slide.is-active');

        $slide.next('.slide').addClass('is-active');
        $('.slide').removeClass('is-behind');
        $('.slide.is-active:not(:last)').addClass('is-behind');
    },
};