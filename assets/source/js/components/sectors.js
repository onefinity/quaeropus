var $ = require('jquery'),
    _ = require('lodash'),
    iWantHue = require('iwanthue-api')();

module.exports = {
    // Colors provided by http://tools.medialab.sciences-po.fr/iwanthue/
    colors: {
        available: iWantHue.diffSort(
            iWantHue.generate(
                global.sectors.length, // Number of colors to generate
                function(color) { // This function filters valid colors...
                    var hcl = color.hcl();
                    return hcl[0] >= 0 && hcl[0] <= 360 && // ...for a specific range of hues
                           hcl[1] >= 0.5 && hcl[1] <= 3 &&
                           hcl[2] >= 0.5 && hcl[2] <= 1.5;
                },
                false, // Use Force Vector (for k-Means, use true)
                50 // Color steps (quality)
            )
        ).map(function(v) { return 'rgb(' + v.rgb.join(',') + ')'; }),
        selected: {}
    },

    init: function() {
        var self = this;

        // Create sector selection
        _.each(global.sectors, function(sector) {
            $('.sector-list').append([
                '<li class="sector ornament">',
                    '<div class="sector-image" style="background-image: url(assets/images/' + sector.sector.toLowerCase() + '.png)"></div>',
                    '<input type="checkbox" name="sector-' + sector.sector.toLowerCase() + '" id="sector-' + sector.sector.toLowerCase() + '">',
                    '<label for="sector-' + sector.sector.toLowerCase() + '">' + sector.title + '</label>',
                '</li>'
            ].join('\n'));
        });

        $('.sector input:checkbox').on('change', function() {
            var $this   = $(this),
                $parent = $this.parent(),
                sector  = $this.attr('name').replace('sector-', '');

            if ( $this.is(':checked') ) {
                $parent.addClass('is-active');

                // Select color
                var color;
                if ( typeof self.colors.selected[sector] === 'undefined' ) {
                    color = _.difference(self.colors.available, _.values(self.colors.selected))[0];
                    self.colors.selected[sector] = color;
                } else {
                    color = self.colors.selected[sector];
                }

                // Generate RGB array, useful in rgba()-cases
                var rgb = color.replace('rgb(', '').replace(')', '').split(',');

                // Inject styling for lines/areas based on generated color
                var css = [
                    '<style type="text/css" class="inject-css-sector-{sector}">',
                        '.sector-color-{sector} { background-color: {color}; }',
                        '.sector-{sector} .area { fill: {color}; opacity: .25; }',
                        '.sector-{sector}.hover .area { fill: {color}; opacity: .4; }',
                        '.sector-{sector} .line { stroke: {color}; }',
                        '.sector-{sector} .floating-bar-background { fill: {color} !important; opacity: .15 !important; }',
                        '.sector-{sector}.hover .floating-bar-background { opacity: .3 !important; }',
                        '.sector-{sector} .floating-bar-area.floating-bar--male { fill: cyan; }',
                        '.sector-{sector} .floating-bar-area.floating-bar--female { fill: pink; }',
                        '.sector-{sector} .floating-bar-line.floating-bar--male { stroke: cyan; }',
                        '.sector-{sector} .floating-bar-line.floating-bar--female { stroke: pink; }',
                    '</style>'
                ].join('')
                 .replace(/\{sector}/g, sector)
                 .replace(/\{color}/g, color)
                 .replace(/\{red}/g, rgb[0])
                 .replace(/\{green}/g, rgb[1])
                 .replace(/\{blue}/g, rgb[2]);
                $(css).appendTo('head');

                // Dynamically create sector color element
                var $div = $('<div class="sector-color sector-color-' + sector + '"></div>').attr('title', 'Use this color to find sector-specific data later on');
                $parent.append($div);
            } else {
                $('head .inject-css-sector-' + sector).remove();
                $parent.removeClass('is-active');
            }

            // Show/hide next-slide button
            if ( $('.sector-list input:checkbox:checked').length >= 2 ) {
                $('#sectors .next-slide').addClass('is-visible');
            } else {
                $('#sectors .next-slide').removeClass('is-visible');
            }

            var sectors = [];
            $('.sector-list input:checkbox:checked').each(function() {
                sectors.push($(this).attr('name').replace('sector-', ''));
            });
            $('#visualizations').attr('data-sectors', sectors.join(','));
        });
    }
};