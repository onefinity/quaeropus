var _ = require('lodash'),
    $ = require('jquery'),
    d3 = require('d3');

module.exports = {
    data: {},
    filters: {},
    margin: {top: 160, right: 140, bottom: 80, left: 150},
    svg: null,

    init: function() {
        var self = this;

        if ( _.isEmpty(this.data) ) {
            d3.json('assets/data/jobs.json', function(err, data) {
                if ( err ) throw err;

                self.data = data;
                self.draw();

                // Filtering
                if ( _.isEmpty(self.filters) ) {
                    self.filter();
                } else {
                    // Update elements to reflect current filters
                    self.filterElements();
                }
            });
        } else {
            this.draw();
            this.filterElements();
        }

        // Event listeners
        $('#pane-jobs .viz-options input').on('change', function() {
            self.filter();
        });
    },
    draw: function() {
        var container = d3.select('#pane-jobs');
        if ( !container.node() ) return false;

        var rect = container.node().getBoundingClientRect(),
            width = rect.width - this.margin.left - this.margin.right,
            height = rect.height - this.margin.top - this.margin.bottom;

        var x = d3.time.scale()
            .range([0, width]);
        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .ticks(d3.time.years)
            .orient('bottom');
        var yAxis = d3.svg.axis()
            .scale(y)
            .ticks(height/100)
            .orient('left');

        this.x = x;
        this.y = y;
        this.xAxis = xAxis;
        this.yAxis = yAxis;

        var svg = container.insert('svg', ':first-child')
            .attr('width', width + this.margin.left + this.margin.right)
            .attr('height', height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + this.margin.left + ', ' + this.margin.top + ')');

        this.svg = d3.select('#pane-jobs svg');

        // Generate areas/lines
        var area = d3.svg.area()
            .interpolate('monotone')
            .x(function(d) { return x(d.year); })
            .y0(function(d) { return y(d.low); })
            .y1(function(d) { return y(d.high); });
        var line_low = d3.svg.line()
            .interpolate('monotone')
            .x(function(d) { return x(d.year); })
            .y(function(d) { return y(d.low); });
        var line_high = d3.svg.line()
            .interpolate('monotone')
            .x(function(d) { return x(d.year); })
            .y(function(d) { return y(d.high); });

        this.area = area;
        this.line_low = line_low;
        this.line_high = line_high;

        var self = this;
        var data = _.map(this.data, function(data, sector) {
            var type = self.filters.type || 'total'; // total/employees/independent
            var gender = self.filters.gender || 'total'; // total/male/female

            return {
                sector: sector,
                values: _.map(data, function(data, year) {
                    return {
                        year: d3.time.format('%Y').parse(year),
                        low: data.workers[type][gender],
                        high: data.jobs[type][gender]
                    };
                })
            };
        });

        // Set y domain to min/max of entire object and x domain to years
        this.updateDomains(data);

        // Create visuals
        var sector = svg.selectAll('.area')
            .data(data)
            .enter()
            .append('g')
            .attr('class', function(d) { return 'sector sector-' + d.sector.toLowerCase(); });

        sector.append('path')
            .attr('class', 'line line-low')
            .attr('d', function(d) { return line_low(d.values); });
        sector.append('path')
            .attr('class', 'line line-high line--dashed')
            .attr('d', function(d) { return line_high(d.values); });
        sector.append('path')
            .attr('class', 'area')
            .attr('d', function(d) { return area(d.values); })
            .on('mouseover', function() {
                var sector = d3.select(this.parentNode).attr('class').toString().match(/sector-[a-z]\b/)[0];
                d3.selectAll('.' + sector).classed('hover', true);
            })
            .on('mouseout', function() {
                var sector = d3.select(this.parentNode).attr('class').toString().match(/sector-[a-z]\b/)[0];
                d3.selectAll('.' + sector).classed('hover', false);
            });

        svg.selectAll('.year')
            .data(function() { return _.sample(data).values; })
            .enter()
            .append('g')
            .attr('class', function(d) { return 'year year-' + d.year.getFullYear(); })
            .append('line')
            .attr('class', 'year-ruler line line--lighter line--dotted')
            .attr({
                y1: 0,
                y2: height
            })
            .attr('x1', function(d) { return x(d.year); })
            .attr('x2', function(d) { return x(d.year); });

        // Labels
        sector.append('text')
            .text(function(d) {
                var title;
                _.each(global.sectors, function(sector) {
                    if ( sector.sector === d.sector.toLowerCase() ) title = sector.title;
                    return;
                });

                return title;
            })
            .attr('class', function(d) { return 'sector-title sector-' + d.sector.toLowerCase(); })
            .attr('x', width/2)
            .attr('y', function(d) {
                var i = Math.round(d.values.length/2) - 1;
                return y(d.values[i].high - ((d.values[i].high - d.values[i].low) / 2));
            })
            .attr('text-anchor', 'middle')
            .attr('dominant-baseline', 'middle');

        // Masks
        svg.append('rect')
            .attr('class', 'mask mask-under')
            .attr('width', width)
            .attr('height', this.margin.bottom)
            .attr('y', height);

        // Axis
        svg.append('g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis)
           .append('text')
            .attr('class', 'label')
            .attr('transform', 'translate(' + width/2 + ', 50)')
            .attr('text-anchor', 'middle')
            .text('Years \u2192');
        svg.append('g')
            .attr('class', 'y axis')
            .call(yAxis)
           .append('g')
            .attr('transform', 'translate(-70, ' + height/2 + ')')
            .attr('class', 'label')
           .append('text')
            .attr('transform', 'rotate(-90)')
            .attr('text-anchor', 'middle')
            .text('# of employees & jobs (\u00D71000) \u2192');
    },
    update: function(type, gender) {
        var x = this.x,
            y = this.y,
            xAxis = this.xAxis,
            yAxis = this.yAxis,
            svg = this.svg,
            area = this.area,
            line_low = this.line_low,
            line_high = this.line_high;

        var data = _.map(this.data, function(data, sector) {
            return {
                sector: sector,
                values: _.map(data, function(data, year) {
                    return {
                        year: d3.time.format('%Y').parse(year),
                        low: data.workers[type][gender],
                        high: data.jobs[type][gender]
                    };
                })
            };
        });

        this.updateDomains(data);

        xAxis.scale(x);
        yAxis.scale(y).ticks(innerHeight/75);
        svg.select('.x.axis').transition().duration(500).call(xAxis);
        svg.select('.y.axis').transition().duration(500).call(yAxis);

        var sectors = svg.selectAll('.sector').data(data);
        sectors.select('.area').transition().duration(500).attr('d', function(d) { return area(d.values); });
        sectors.select('.line-high').transition().duration(500).attr('d', function(d) { return line_high(d.values); });
        sectors.select('.line-low').transition().duration(500).attr('d', function(d) { return line_low(d.values); });
        sectors.select('.sector-title').transition().duration(500).attr('y', function(d) {
            var i = Math.round(d.values.length/2) - 1;
            return y(d.values[i].high - ((d.values[i].high - d.values[i].low) / 2));
        });
    },
    updateDomains: function(data) {
        // Set y domain to min/max of entire object and x domain to years
        this.x.domain(d3.extent(_.keys(_.sample(this.data)).map(function(d) { return d3.time.format('%Y').parse(d); })));
        if ( this.filters.zoom ) {
            var s = $('#visualizations').attr('data-sectors');

            if ( typeof s !== 'undefined' ) {
                var selectedSectors = s.split(',');
                var filteredData = _.filter(data, function(sector) { return selectedSectors.indexOf(sector.sector.toLowerCase()) >= 0; });
                this.y.domain(d3.extent(_.filter(_.crush(filteredData), function(v) { return !isNaN(v); })));
            }
        } else {
            this.y.domain(d3.extent(_.filter(_.crush(data), function(v) { return !isNaN(v); })));
        }
    },
    resize: function() {
        var x = this.x,
            y = this.y,
            xAxis = this.xAxis,
            yAxis = this.yAxis,
            svg = this.svg,
            area = this.area,
            line_low = this.line_low,
            line_high = this.line_high,
            container = d3.select('#pane-jobs');
        if ( !container.node() ) return false;

        var rect = container.node().getBoundingClientRect(),
            width = rect.width,
            height = rect.height,
            innerWidth = width - this.margin.left - this.margin.right,
            innerHeight = height - this.margin.top - this.margin.bottom;

        // Update ranges
        x.range([0, innerWidth]);
        y.range([innerHeight, 0]);

        // Update data
        svg.selectAll('.line-low').attr('d', function(d) { return line_low(d.values); });
        svg.selectAll('.line-high').attr('d', function(d) { return line_high(d.values); });
        svg.selectAll('.area').attr('d', function(d) { return area(d.values); });
        svg.selectAll('.sector-title').attr('y', function(d) {
            var i = Math.round(d.values.length/2) - 1;
            return y(d.values[i].high - ((d.values[i].high - d.values[i].low) / 2));
        });

        // Update axis scales
        xAxis.scale(x);
        yAxis.scale(y).ticks(innerHeight/75);

        // Update masks
        svg.select('.mask-under')
            .attr('y', innerHeight);

        // Update x/y axis
        svg.select('.x.axis')
            .attr('transform', 'translate(0, ' + innerHeight + ')')
            .call(xAxis)
           .select('.label')
            .attr('transform', 'translate(' + innerWidth/2 + ', 60)');
        svg.select('.y.axis')
            .call(yAxis)
            .select('.label')
            .attr('transform', 'translate(-70, ' + innerHeight/2 + ')');

        // Update width/height of SVG element
        this.svg.attr('width', width);
        this.svg.attr('height', height);
    },
    filter: function() {
        _.extend(this.filters, {
            zoom: $('.panes-clone input[name="jobs-zoom"]').is(':checked'),
            type: $('.panes-clone input[name="jobs-type"]:checked').val(),
            gender: $('.panes-clone input[name="jobs-gender"]:checked').val()
        });

        this.update(this.filters.type, this.filters.gender);
    },
    filterElements: function() {
        $('.panes-clone input[name="jobs-zoom"]').prop('checked', this.filters.zoom);
        $('.panes-clone input[name="jobs-type"][value="' + this.filters.type + '"]').prop('checked', true);
        $('.panes-clone input[name="jobs-gender"][value="' + this.filters.gender + '"]').prop('checked', true);
    }
};