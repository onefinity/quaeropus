var $ = require('jquery');

module.exports = {
    init: function() {
        var $intro = $('#intro'),
            v = $intro.find('video')[0];

        $intro.find('.skip-intro').on('click', function(e) {
            e.preventDefault();
            $intro.removeClass('is-video');
        });

        $intro.removeClass('is-loading').addClass('is-video');

        v.play();
        v.onended = function() {
            $intro.removeClass('is-video');
        };
    }
};