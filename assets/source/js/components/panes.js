// Split.js is included at the bottom of index.html since it's not an NPM module
var $ = require('jquery'),
    _ = require('lodash');
var visualizations = require('./visualizations.js');

module.exports = {
    options: {
        direction: 'vertical',
        snapOffset: 1,
        gutterSize: 12,
        minSize: 400
    },
    throttle: 10, // How much events should be throttled in ms

    init: function() {
        var self = this,
            $container = $('#visualizations');

        // Clone panes container
        var $panes = $container.find('.panes');
        $panes.after($panes.clone(true, true).addClass('panes-clone')).addClass('panes-original').hide();

        // Show all panes and hide the not-checked again to make sure split.js works correctly
        var $closed = $container.find('.pane-options input:checkbox:not(:checked)');
        $container.find('.pane-options input:checkbox').prop('checked', true);
        setTimeout(function() {
            $closed.trigger('click');
        });

        // Initial update()
        this.update();

        // Attach event listeners
        $container.find('.pane-close').on('click', function(e) {
            e.preventDefault();

            // Toggle checkbox
            var panel = $(this).closest('.pane').attr('id').replace('pane-', 'pane-options-');
            $('#' + panel).trigger('click');

            self.update();
        });
        $container.find('.pane-options input:checkbox').on('change', function() {
            self.update();
        });

        $(window).on('resize', _.throttle(visualizations.resize, self.throttle));
    },
    update: function() {
        var self = this,
            $container = $('#visualizations');

        // Determine which panes are open
        var panes = [];
        $container.find('.pane-options input:checkbox').each(function() {
            var $this = $(this);
            if ( $this.is(':checked') ) panes.push('pane-' + $this.attr('name'));
        });

        // Update [data-visible] attribute on parent
        $container.find('.panes-clone').attr('data-visible', panes.length);

        // Add multiple-panes-visible class to container if necessary
        if ( panes.length > 1 ) {
            $container.addClass('multiple-panes-visible');
        } else {
            $container.removeClass('multiple-panes-visible');
        }

        // Update pane .is-visible classes
        var $panes = $container.find('.panes-clone .pane');
        var open = [];
        $panes.each(function() {
            open.push($(this).attr('id'));
        });
        $container.find('.panes-clone').empty();

        _.each(panes, function(pane) {
            // Create clone for each visible panel
            var $clone = $container.find('.panes-original [data-id="' + pane + '"]').clone(true, true); // Clone events/elements from element + children
            var id = $clone.attr('data-id');

            // Clean up cloned panel
            $clone.attr('id', id).removeAttr('data-id');

            // Fix checked checkbox/radio inputs
            $clone.find('input:radio[checked], input:checkbox[checked]').prop('checked', true);

            // Add .is-redrawing class to existing panes and add .is-loading class only to new panes
            $clone.addClass('is-redrawing');
            if ( !_.contains(open, id) ) $clone.addClass('is-loading');

            // Append panes
            $container.find('.panes-clone').append($clone);
        }.bind(this));

        // Remove .is-loading classes from panes
        setTimeout(function() {
            $container.find('.panes-clone .pane').removeClass('is-loading is-redrawing');
        });

        // Split
        Split(panes, _.extend(this.options, {
            sizes: _.range(panes.length).map(function() { return 100/panes.length; }),
            onDrag: _.throttle(visualizations.resize, self.throttle)
        }));

        // Initialize visualizations
        visualizations.init();
    }
};