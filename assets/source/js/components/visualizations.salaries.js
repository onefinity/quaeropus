var _ = require('lodash'),
    $ = require('jquery'),
    d3 = require('d3');

require('d3-tip')(d3);

module.exports = {
    data: {},
    filters: {},
    margin: {top: 160, right: 140, bottom: 80, left: 150},
    svg: null,

    init: function() {
        var self = this;

        // Load data if it hasn't been loaded already
        if ( _.isEmpty(this.data) ) {
            d3.json('assets/data/salaries.json', function(err, data) {
                if ( err ) throw err;

                self.data = data;
                self.draw();

                // Filtering
                if ( _.isEmpty(self.filters) ) {
                    self.filter();
                } else {
                    // Update elements to reflect current filters
                    self.filterElements();
                }
            });
        } else {
            this.draw();
            this.filterElements();
        }

        // Event listeners
        $('#pane-salaries .viz-options input').on('change', function() {
            self.filter();
        });
    },
    draw: function() {
        var container = d3.select('#pane-salaries');
        if ( !container.node() ) return false;

        var rect = container.node().getBoundingClientRect(),
            width = rect.width - this.margin.left - this.margin.right,
            height = rect.height - this.margin.top - this.margin.bottom;

        var x0 = d3.scale.ordinal()
            .rangeBands([0, width]);
        var x1 = d3.scale.ordinal();
        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x0)
            .ticks(d3.time.years)
            .orient('bottom');
        var yAxis = d3.svg.axis()
            .scale(y)
            .ticks(height/75)
            .orient('left');

        this.x0 = x0;
        this.x1 = x1;
        this.y = y;
        this.xAxis = xAxis;
        this.yAxis = yAxis;

        var svg = container.insert('svg', ':first-child')
            .attr('width', width + this.margin.left + this.margin.right)
            .attr('height', height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + this.margin.left + ', ' + this.margin.top + ')');

        this.svg = d3.select('#pane-salaries svg');

        var data = _.map(this.data, function(data, year) {
            return {
                year: d3.time.format('%Y').parse(year),
                values: _.map(data, function(data, sector) {
                    return {
                        sector: sector,
                        parttime: {
                            male: {
                                low: data.parttime.male.exclusive,
                                high: data.parttime.male.inclusive
                            },
                            female: {
                                low: data.parttime.female.exclusive,
                                high: data.parttime.female.inclusive
                            }
                        },
                        fulltime: {
                            male: {
                                low: data.fulltime.male.exclusive,
                                high: data.fulltime.male.inclusive
                            },
                            female: {
                                low: data.fulltime.female.exclusive,
                                high: data.fulltime.female.inclusive
                            }
                        }
                    };
                })
            };
        });
        this.mappedData = data;

        x0.domain(_.keys(this.data));
        x1.domain(_.keys(_.sample(this.data))).rangeBands([0, x0.rangeBand()]);
        y.domain([0, d3.max(_.crush(this.data))]);

        //
        var ruler = svg.append('line')
            .attr('class', 'horizontal-ruler line line--lighter line--dotted')
            .attr({
                x1: 0,
                y1: 100,
                x2: width,
                y2: 100
            });

        svg.on('mousemove', function() {
            var m = d3.mouse(svg.node());
            ruler.classed('is-active', true)
                .attr({
                    y1: m[1],
                    y2: m[1]
                });
        });
        svg.on('mouseleave', function() {
            ruler.classed('is-active', false);
        });

        svg.append('g')
            .attr('class', 'titles')
            .selectAll('.title')
            .data(global.sectors)
            .enter().append('text')
            .text(function(d) { return d.title; })
            .attr('class', function(d) { return 'sector-title sector-' + d.sector; })
            .attr('text-anchor', 'middle')
            .attr('x', width/2)
            .attr('y', -15);

        var year = svg.selectAll('.year')
            .data(data)
           .enter().append('g')
            .attr('class', function(d) { return 'year year-' + d.year.getFullYear(); })
            .attr('transform', function(d) { return 'translate(' + x0(d.year.getFullYear()) + ', 0)'; })
            .on('mouseover', function() {
                var year = d3.select(this).attr('class').toString().match(/year-\d{4}\b/)[0];
                d3.selectAll('.' + year).classed('hover', true);
            })
            .on('mouseout', function() {
                var year = d3.select(this).attr('class').toString().match(/year-\d{4}\b/)[0];
                d3.selectAll('.' + year).classed('hover', false);
            });

        year.append('line')
            .attr('class', 'year-seperator line line--lighter line--dotted')
            .attr({
                x1: width/_.keys(this.data).length,
                y1: 0,
                x2: width/_.keys(this.data).length,
                y2: height
            });

        var sector = year.selectAll('.sector')
            .data(function(d) { return d.values; })
           .enter().append('g')
            .attr('class', function(d) { return 'sector sector-' + d.sector.toLowerCase(); })
            .on('mouseover', function() {
                var sector = d3.select(this).attr('class').toString().match(/sector-[a-z]\b/)[0];
                d3.selectAll('.' + sector).classed('hover', true);
            })
            .on('mouseout', function() {
                var sector = d3.select(this).attr('class').toString().match(/sector-[a-z]\b/)[0];
                d3.selectAll('.' + sector).classed('hover', false);
            });

        sector.append('rect')
            .attr('class', 'floating-bar-background')
            .attr('width', x1.rangeBand())
            .attr('height', height)
            .attr('x', function(d) { return x1(d.sector); })
            .attr('y', 0);

        var tips = sector.append('g')
            .attr('class', 'floating-bar-tips')
            .attr('width', x1.rangeBand())
            .attr('height', height)
            .attr('x', function(d) { return x1(d.sector); })
            .attr('y', 0);

        var padding = 8;
        _.each(['parttime', 'fulltime'], function(time) {
            _.each(['male', 'female'], function(gender) {
                sector.append('rect')
                    .attr('class', 'floating-bar floating-bar-area floating-bar--' + time + ' floating-bar--' + gender)
                    .attr('width', x1.rangeBand())
                    .attr('height', function(d) { return height - y(d[time][gender].high - d[time][gender].low); })
                    .attr('x', function(d) { return x1(d.sector); })
                    .attr('y', function(d) { return y(d[time][gender].low); });

                sector.append('line')
                    .attr('class', 'floating-bar-line floating-bar--' + time + ' floating-bar--' + gender)
                    .attr({
                        x1: function(d) { return x1(d.sector); },
                        y1: function(d) { return y((2 * d[time][gender].low) - d[time][gender].high); },
                        x2: function(d) { return x1(d.sector) + x1.rangeBand(); },
                        y2: function(d) { return y((2 * d[time][gender].low) - d[time][gender].high); }
                    });

                tips.append('text')
                    .text(function(d) { return '€' + d[time][gender].low; })
                    .attr({
                        x: function(d) { return ( gender === 'male' ) ? x1(d.sector) - padding : x1(d.sector) + x1.rangeBand() + padding; },
                        y: function(d) { return y((2 * d[time][gender].low) - d[time][gender].high); },
                        class: ( gender === 'male' ) ? 'floating-bar-tip floating-bar-tip--left' : 'floating-bar-tip floating-bar-tip--right'
                    })
                    .attr('text-anchor', function() {
                        return ( gender === 'male' ) ? 'end' : 'start';
                    });
            });
        });

        // Masks
        svg.append('rect')
            .attr('class', 'mask mask-under')
            .attr('width', width)
            .attr('height', this.margin.bottom)
            .attr('y', height);

        // Draw axis
        svg.append('g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis)
            .append('text')
            .attr('class', 'label')
            .attr('transform', 'translate(' + width/2 + ', 60)')
            .attr('text-anchor', 'middle')
            .text('Years \u2192');
        svg.append('g')
            .attr('class', 'y axis')
            .call(yAxis)
            .append('g')
            .attr('transform', 'translate(-70, ' + height/2 + ')')
            .attr('class', 'label')
            .append('text')
            .attr('transform', 'rotate(-90)')
            .attr('text-anchor', 'middle')
            .text('Monthly average salary in \u20AC \u2192');
    },
    update: function() {
        var data = this.mappedData,
            x0 = this.x0,
            x1 = this.x1,
            y = this.y,
            xAxis = this.xAxis,
            yAxis = this.yAxis,
            svg = this.svg,
            filters = this.filters,
            container = d3.select('#pane-salaries');
        if ( !container.node() ) return false;

        var rect = container.node().getBoundingClientRect(),
            width = rect.width,
            height = rect.height,
            innerWidth = width - this.margin.left - this.margin.right,
            innerHeight = height - this.margin.top - this.margin.bottom;

        // Zoom
        if ( filters.zoom ) {
            var filteredData = [],
                s = $('#visualizations').attr('data-sectors');
            if ( typeof s !== 'undefined' ) {
                var selectedSectors = s.split(',');

                _.each(this.data, function(sectors) {
                    _.each(sectors, function(times, sector) {
                        if ( selectedSectors.indexOf(sector.toLowerCase()) >= 0 ) {
                            filteredData.push(times);
                        }
                    });
                });
            }

            y.domain([0, d3.max(_.crush(filteredData))]);
        } else {
            y.domain([0, d3.max(_.crush(this.data))]);
        }

        xAxis.scale(x0);
        yAxis.scale(y).ticks(innerHeight/75);
        svg.select('.x.axis').transition().duration(500).call(xAxis);
        svg.select('.y.axis').transition().duration(500).call(yAxis);

        // Visuals
        _.each(['parttime', 'fulltime'], function(time) {
            _.each(['male', 'female'], function(gender) {
                svg.selectAll('.floating-bar-area.floating-bar--' + time + '.floating-bar--' + gender)
                    .transition().duration(500)
                    .attr('height', function(d) { return innerHeight - y(d[time][gender].high - d[time][gender].low); })
                    .attr('y', function(d) { return y(d[time][gender].low); });

                svg.selectAll('.floating-bar-line.floating-bar--' + time + '.floating-bar--' + gender)
                    .transition().duration(500)
                    .attr('y1', function(d) { return y((2 * d[time][gender].low) - d[time][gender].high); })
                    .attr('y2', function(d) { return y((2 * d[time][gender].low) - d[time][gender].high); });
            });
        });

        // Gender
        svg.selectAll('.floating-bar--female.floating-bar--parttime').style('visibility', function() { return ( ['total', 'female'].indexOf(filters.gender) >= 0 && ['total', 'parttime'].indexOf(filters.time) >= 0 ) ? 'visible' : 'hidden'; });
        svg.selectAll('.floating-bar--female.floating-bar--fulltime').style('visibility', function() { return ( ['total', 'female'].indexOf(filters.gender) >= 0 && ['total', 'fulltime'].indexOf(filters.time) >= 0 ) ? 'visible' : 'hidden'; });
        svg.selectAll('.floating-bar--male.floating-bar--parttime').style('visibility', function() { return ( ['total', 'male'].indexOf(filters.gender) >= 0 && ['total', 'parttime'].indexOf(filters.time) >= 0 ) ? 'visible' : 'hidden'; });
        svg.selectAll('.floating-bar--male.floating-bar--fulltime').style('visibility', function() { return ( ['total', 'male'].indexOf(filters.gender) >= 0 && ['total', 'fulltime'].indexOf(filters.time) >= 0 ) ? 'visible' : 'hidden'; });

        // Hide/show overtime
        svg.selectAll('.floating-bar-area').style('opacity', function() { return ( filters.hideovertime ) ? 0 : 0.2; });
    },
    resize: function() {
        var x0 = this.x0,
            x1 = this.x1,
            y = this.y,
            xAxis = this.xAxis,
            yAxis = this.yAxis,
            svg = this.svg,
            container = d3.select('#pane-salaries');
        if ( !container.node() ) return false;

        var rect = container.node().getBoundingClientRect(),
            width = rect.width,
            height = rect.height,
            innerWidth = width - this.margin.left - this.margin.right,
            innerHeight = height - this.margin.top - this.margin.bottom;

        // Update ranges
        x0.rangeBands([0, innerWidth]);
        y.range([innerHeight, 0]);

        // Update masks
        svg.select('.mask-under')
            .attr('y', innerHeight);

        // Update data
        svg.selectAll('.year').attr('transform', function(d) { return 'translate(' + x0(d.year.getFullYear()) + ', 0)'; });
        svg.selectAll('.year-seperator').attr('y2', innerHeight);
        svg.selectAll('.floating-bar-background')
            .attr('width', x1.rangeBand())
            .attr('height', innerHeight);

        _.each(['parttime', 'fulltime'], function(time) {
            _.each(['male', 'female'], function(gender) {
                svg.selectAll('.floating-bar-area.floating-bar--' + time + '.floating-bar--' + gender)
                    .attr('width', x1.rangeBand())
                    .attr('height', function(d) { return innerHeight - y(d[time][gender].high - d[time][gender].low); })
                    .attr('x', function(d) { return x1(d.sector); })
                    .attr('y', function(d) { return y(d[time][gender].low); });
                svg.selectAll('.floating-bar-line.floating-bar--' + time + '.floating-bar--' + gender)
                    .attr({
                        x1: function(d) { return x1(d.sector); },
                        y1: function(d) { return y((2 * d[time][gender].low) - d[time][gender].high); },
                        x2: function(d) { return x1(d.sector) + x1.rangeBand(); },
                        y2: function(d) { return y((2 * d[time][gender].low) - d[time][gender].high); }
                    });
            });
        });

        // Update axis scales
        xAxis.scale(x0);
        yAxis.scale(y).ticks(innerHeight/75);

        // Update x/y axis
        this.svg.select('.x.axis')
            .attr('transform', 'translate(0, ' + innerHeight + ')')
            .call(xAxis)
            .select('.label')
            .attr('transform', 'translate(' + innerWidth/2 + ', 60)');
        this.svg.select('.y.axis')
            .call(yAxis)
            .select('.label')
            .attr('transform', 'translate(-70, ' + innerHeight/2 + ')');

        // Update width/height of SVG element
        svg.attr('width', width);
        svg.attr('height', height);
    },
    filter: function() {
        _.extend(this.filters, {
            zoom: $('.panes-clone input[name="salaries-zoom"]').is(':checked'),
            hideovertime: $('.panes-clone input[name="salaries-hideovertime"]').is(':checked'),
            time: $('.panes-clone input[name="salaries-time"]:checked').val(),
            gender: $('.panes-clone input[name="salaries-gender"]:checked').val()
        });

        this.update();
    },
    filterElements: function() {
        $('.panes-clone input[name="salaries-zoom"]').prop('checked', this.filters.zoom);
        $('.panes-clone input[name="salaries-hideovertime"]').prop('checked', this.filters.hideovertime);
        $('.panes-clone input[name="salaries-time"][value="' + this.filters.time + '"]').prop('checked', true);
        $('.panes-clone input[name="salaries-gender"][value="' + this.filters.gender + '"]').prop('checked', true);
    }
};