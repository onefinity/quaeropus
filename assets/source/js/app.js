var $ = require('jquery'),
    keycharm = require('keycharm')(),
    intro = require('./components/intro.js'),
    panes = require('./components/panes.js'),
    slides = require('./components/slides.js'),
    sectors = require('./components/sectors.js');

// Define global DEBUG variable
global.DEBUG = location.search.indexOf('DEBUG') >= 0;
global.sectors = [
    {
        sector: 'a',
        title: 'Agriculture, forestry and fishing',
    },
    {
        sector: 'b',
        title: 'Mining and quarrying',
    },
    {
        sector: 'c',
        title: 'Manufacturing',
    },
    {
        sector: 'd',
        title: 'Electricity and gas',
    },
    {
        sector: 'e',
        title: 'Water supply, management, and remediation',
    },
    {
        sector: 'f',
        title: 'Construction',
    },
    {
        sector: 'g',
        title: 'Wholesale and retail trade',
    },
    {
        sector: 'h',
        title: 'Transportation and storage',
    },
    {
        sector: 'i',
        title: 'Accommodation and food service',
    },
    {
        sector: 'j',
        title: 'Information and communication',
    },
    {
        sector: 'k',
        title: 'Financial institutions',
    },
    {
        sector: 'l',
        title: 'Real estate',
    },
    {
        sector: 'm',
        title: 'Consultancy and research',
    },
    {
        sector: 'n',
        title: 'Tangible goods and business support services',
    },
    {
        sector: 'o',
        title: 'Public administration and services',
    },
    {
        sector: 'p',
        title: 'Education',
    },
    {
        sector: 'q',
        title: 'Human health and social work',
    },
    {
        sector: 'r',
        title: 'Culture, sports and recreation',
    },
    {
        sector: 's',
        title: 'Other service activities'
    }
];

$(function() {
    intro.init();
    sectors.init();
    panes.init();
    slides.init();

    $('body').removeClass('loading');

    // Bind global keys
    keycharm.bind('r', function() {
        // Needed for electron kiosk mode
        $('body').addClass('is-reloading');
        window.location.reload();
    });
});